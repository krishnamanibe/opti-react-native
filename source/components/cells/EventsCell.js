import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity,
} from 'react-native';

import moment from 'moment';
const deviceWidth = Dimensions.get('window').width;
class EventCell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isExpand: false,
        }
    }

    _renderRowData = (rowData, sectionID, rowID) => {
        const {
            isExpand,
        } = this.state;
        if (isExpand) {
            return (
                <View style={Styles.rowContainer}>
                    <View style={{ flex: 1 }}>
                        <Image
                            source={{ uri: 'http://d25jwrqpnjo0xm.cloudfront.net/94/60141539626685651.jpg' }}
                            style={Styles.rowImage}

                        />
                    </View>
                    <View style={{ flex: 4 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text numberOfLines={1}>{rowData.event_name}</Text>
                            <Text>{moment(rowData.event_created_at).format('DD MMMM')}</Text>
                        </View>
                        <View style={{alignItems:'center', justifyContent:'center'}}>
                        <Image
                            source={{ uri: 'http://d25jwrqpnjo0xm.cloudfront.net/94/60141539626685651.jpg' }}
                            style={{ width: deviceWidth , height: deviceWidth/2}}
                            resizeMode={'contain'}

                        />
                        </View>
                        <Text numberOfLines={1}>{'Velachery, Chennai'}</Text>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={Styles.rowContainer}>
                    <View style={{ flex: 1 }}>
                        <Image
                            source={{ uri: 'http://d25jwrqpnjo0xm.cloudfront.net/94/60141539626685651.jpg' }}
                            style={Styles.rowImage}

                        />
                    </View>
                    <View style={{ flex: 4 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text numberOfLines={1}>{rowData.event_name}</Text>
                            <Text>{moment(rowData.event_created_at).format('DD MMMM')}</Text>
                        </View>
                        <Text numberOfLines={1}>{'Velachery, Chennai'}</Text>
                    </View>
                </View>
            )
        }
    }
    render() {
        const {
            rowData, sectionID, rowID
        } = this.props;
        const {
            isExpand
        } = this.state;
        return (
        <TouchableOpacity style={{flex:1}} onPress={()=>{
            this.setState({
                isExpand: !isExpand,
            },()=>{
                this.props.onPress(isExpand);
            })
        }}>
{this._renderRowData(rowData, sectionID, rowID)}
        </TouchableOpacity>
        )
    }
}



export default EventCell;

const Styles = StyleSheet.create({
    rowContainer: {
        padding: 10,
        flexDirection: 'row',
        borderBottomColor: '#000',
        borderBottomWidth: 1,
    },
    rowImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
    }

});