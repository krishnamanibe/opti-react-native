import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

const deviceHeight = Dimensions.get('window').height; 

class SplashScreen extends Component {

    componentDidMount = ()=>{
        setTimeout(() => {
            Actions.push('DashboardScreen')
        }, 2000);
    }

    render() {
        return (
            <View
                style={Styles.container}
            >
                <Image
                    resizeMode={'contain'}
                    source={require('../images/logo.png')}
                    style={{ width: deviceHeight / 3, height: deviceHeight / 3 }}

                />
            </View>
        )
    }
}


const mapStateToProps = (state, props) => {
    return {};
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    }
})