import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    ListView,
    ToastAndroid,
    ActivityIndicator,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getListWebservice, showToastMessage } from '../../actions/DashboardAction';
import EventCell from '../cells/EventsCell';

const deviceHeight = Dimensions.get('window').height;

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class DashboardScreen extends Component {

    constructor(props) {
        super(props);
        this._renderRow = this._renderRow.bind(this);

        this.state = {
            isLoading: true,
        }
    }

    componentDidMount = () => {
        this.setState({
            isLoading: true,
        },()=>{
            
        this.props.getListWebservice((response) => {

            this.setState({
                isLoading: false,
            });
            if (!response) {
                ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
            }
        });

    });
    }

    _renderRow(rowData, sectionID, rowID) {


        return (
            <EventCell
                onPress={(status) => {
                    this.props.showToastMessage(status);
                }}
                rowData={rowData}
                sectionID={sectionID}
                rowID={rowID}
            />
        )
    }

    render() {
        return (
            <View
                style={Styles.container}
            >
                {
                    this.state.isLoading ?
                        <View style={{ flex: 1, alignContent:'center', justifyContent:'center'}}>
                            <ActivityIndicator
                                style={Styles.activityIndicator}
                                size={'large'}
                                color={'blue'}
                            />
                        </View>
                        :

                        <ListView
                            dataSource={ds.cloneWithRows(this.props.listJSONArray)}
                            enableEmptySections={true}
                            renderRow={this._renderRow}
                        />
                }
            </View>
        )
    }
}


const mapStateToProps = (state, props) => {
    return {
        listJSONArray: state.dashboardState.listJSONArray,
    };
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getListWebservice,
        showToastMessage,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }, borderRadius: 100,

    activityIndicator: {
        margin: 'auto',
        zIndex: 9,
    },
})