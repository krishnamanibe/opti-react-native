/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Store from './Store';
import Route from './Route'

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <Route />
      </Provider>
    );
  }
}
