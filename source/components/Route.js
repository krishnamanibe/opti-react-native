import React, { Component } from 'react';
import { Router, Scene, ActionConst, Stack, Tabs } from 'react-native-router-flux';
import SplashScreen from './screens/SplashScreen';
import { View } from 'react-native';
import DashboardScreen from './screens/DashboardScreen';
class Route extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Router >

                    <Stack key="root" 
            hideNavBar>
                        <Scene key={'SplashScreen'}
                            component={SplashScreen}
                            type={ActionConst.RESET}
                            initial
                        />
                        <Scene key={'DashboardScreen'}
                          component={DashboardScreen}
                          type={ActionConst.RESET}
                          renderBackButton={()=>(null)}
                        //   hideNavBar
                        />
                    </Stack>
                </Router>
            </View>
        )
    }
}

export default Route;