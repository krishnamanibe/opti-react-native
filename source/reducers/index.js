
import { combineReducers } from 'redux';
import { dashboardState} from './DashboardReducers';
const rootReducer = combineReducers({
    dashboardState,
});


export default rootReducer;