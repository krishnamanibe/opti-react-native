import { ACTIONS } from "../components/utils/ActionConstants";

let initialState = {
    listJSONArray : [],
}
export const dashboardState = (state = initialState, action) => {
    console.log(' action.payload', action.payload);
    
    switch (action.type) {
        case ACTIONS.DASHBOARD_GET_SCUESS:
            return {
                ...state,
                listJSONArray: action.payload.data,
            }
    
        default:
        return state;
    }
}