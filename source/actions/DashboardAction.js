

import axios from 'axios';
import { ACTIONS } from '../components/utils/ActionConstants';

import {
  ToastAndroid,
} from 'react-native';

export const getListWebservice = (callback)=>{
    return (dispatch)=>{
       const headers = {
            'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ijk0IiwiaWF0IjoxNTQwMjgzNjMwLCJleHAiOjE1NDAyOTQ0MzB9.kueXkjcVJZMwCVozZ9ZSdm2iiSueLz3xwQDwEPsfVm4',
          };

          const params = {

          }
        const config = {
            method: 'GET',
            url:'/event/event_getAll',
            params,
            headers,
          };

          
      axios.create({
        baseURL: 'http://34.238.95.101:3500',
        timeout: 60000,
      })(config)
      .then((response)=>{

        console.log('response', response);

        if (response.status === 200) {
            dispatch(customAction(ACTIONS.DASHBOARD_GET_SCUESS,response.data))
            callback(true);
        }else{
            callback(false);
        }
      }).catch((error)=>{
        console.log('err', error);
        
      });



    }
}
export function showToastMessage(status = false) {
  return ()=>{
    let text = 'Row closed !';
    if (!status) {
      text = 'Row opened !';
    }
    ToastAndroid.show(text, ToastAndroid.SHORT);
  }
}
export function customAction(action, data) {
    return {
      type: action,
      payload: data,
    };
  }
  